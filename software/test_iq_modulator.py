"""IQ Modulator Interface Tests"""

from crc import Calculator, Crc16
from time import sleep

from iq_modulator_mock import IQModulator
from console_tool import create_message

iq_modulator = IQModulator()

RUN_ON_HW = False

if RUN_ON_HW:
    from serial import Serial
    ser = Serial('/dev/ttyUSB0', baudrate=57600)
else:
    ser = None


def write_to_serial(msg_bytes: bytes):
    """Serial write function"""
    if RUN_ON_HW:
        ser.write(msg_bytes)
    else:
        for b in msg_bytes:
            iq_modulator.read_byte(b.to_bytes(length=1, byteorder='little'))


def read_from_serial():
    """Serial read function"""
    if RUN_ON_HW:
        return ser.read_all()
    else:
        msg_bytes = bytes()
        while True:
            b = iq_modulator.write_byte()
            if b is None:
                break
            msg_bytes += b
        return msg_bytes


def test_valid_packet_lengths():
    """Test that valid packet lengths are handled correctly"""
    payload_lengths = [0, 1, 10, 100, 1000, 1500, 7264, 10000]
    # (10110100)in base 2 = (B4) in base 16
    symbols = bytes.fromhex("B4")
    symbols_bit_str = f"{bin(int.from_bytes(symbols, byteorder='little'))}"
    print(f"Using 8-bit pattern: {symbols_bit_str} ({symbols})")

    for payload_length in payload_lengths:
        print(f"Testing payload length {payload_length}...")
        write_to_serial(create_message(symbols, payload_length))
        response = read_from_serial()
        expected_response = b"Payload size received\r\nTransferred to fabric\r\n"
        if response == expected_response:
            print(f"Payload length {payload_length} OK")
        else:
            print(f"Unexpected response: {response} Expected: {expected_response}")
            assert False


def test_too_long_packet_length():
    """Test that too long packet is ignored"""
    write_to_serial(create_message(bytes.fromhex("B4"), 10001))
    sleep(0.1)
    response = read_from_serial()
    expected_response = b'Bad payload size\r\n'
    if response == expected_response:
        print("Too long payload size reported correctly")
    else:
        print(f"Unexpected response: {response} Expected: {expected_response}")
        assert False


if __name__ == "__main__":
    test_valid_packet_lengths()
    test_too_long_packet_length()

if RUN_ON_HW:
    ser.close()
