import serial
import argparse
from crc import Calculator, Crc16

def check_crc(payload_bytes, crc_bytes):
    calculator = Calculator(Crc16.CCITT)
    return calculator.verify(
        payload_bytes, expected=int.from_bytes(crc_bytes, byteorder="little")
    )


def create_message(payload_bytes):
    """Create message with header"""
    header_bytes = int.to_bytes(len(payload_bytes), length=2, byteorder="little")
    calculator = Calculator(Crc16.CCITT)
    checksum = calculator.checksum(header_bytes + payload_bytes)
    crc_bytes = checksum.to_bytes(length=2, byteorder='little')
    msg_bytes = header_bytes + payload_bytes + crc_bytes

    if len(msg_bytes) <= 10:
        msg_str = f"{msg_bytes}"
    else:
        msg_str = f"{msg_bytes[0:5]} ... {msg_bytes[-5:]}"

    header_int = int.from_bytes(msg_bytes[0:2], byteorder="little")
    assert header_int == len(payload_bytes)
    print(f"Message bytes (message length={len(msg_bytes)}, payload length={len(payload_bytes)}): {msg_str}")
    return msg_bytes


def loop(ser):
    """Main loop"""
    try:
        while True:
            payload = input("Enter the payload: ")
            ser.write(create_message(payload.encode('ASCII')))
            expected_response = b"Payload size received\r\nTransferred to fabric\r\n"
            response = ser.read(len(expected_response))
            print(response)
    except (KeyboardInterrupt, EOFError):
        return


def main():
    parser = argparse.ArgumentParser(description="IQ Modulator Console Interface")
    parser.add_argument("-p", "--port", help="Serial port name", required=True)
    parser.add_argument("-b", "--baudrate", type=int, help="Baudrate", required=True)
    args = parser.parse_args()
    ser = serial.Serial(args.port, args.baudrate)
    loop(ser)
    ser.close()

if __name__ == "__main__":
    main()
