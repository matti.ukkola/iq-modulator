# Software

## Installation

```bash
pip install crc pyserial
```

## Usage

Set RUN_ON_HW to True in `test_iq_modulator.py` to run the test on the FPGA.
This requires that the device is connected to the computer.

```bash
python3 test_iq_modulator.py
```

To run the console tool, run:

```bash
python3 console_tool.py --port /dev/ttyUSB0 --baudrate 57600
```
