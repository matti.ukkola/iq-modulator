#include <stdint.h>
#include <stddef.h>
#include <string.h>

#include "CMSIS/system_m2sxxx.h"
#include "drivers/mss_uart/mss_uart.h"

void print(const uint8_t *msg, size_t size) {
    MSS_UART_polled_tx(&g_mss_uart0, msg, size);
}

void transfer_32_bit_to_fabric(uint32_t data) {
    volatile uint32_t *addr = (uint32_t *)0x30000000;  //the address for APB slave
    *addr = data;
}

size_t transfer_to_fabric(const uint8_t *buffer, const size_t size) {
    size_t sent_bytes = 0;
    while (sent_bytes < size) {
        uint32_t next_4_bytes = 0;
        size_t bytes_left = size - sent_bytes;
        size_t transfer_size = (bytes_left < 4) ? bytes_left : 4;
        memcpy(&next_4_bytes, &buffer[sent_bytes], transfer_size);
        // TODO: add rate limitation
        transfer_32_bit_to_fabric(next_4_bytes);
        sent_bytes += sizeof(next_4_bytes);
    }
    return sent_bytes;
}

int crc_ok(uint16_t crc) {
    // TODO: implement CRC check
    (void)crc;
    return 1;
}

uint16_t payload_size = 0;
uint8_t read_buf[sizeof(payload_size) + 10000 + sizeof(uint16_t)] = {};
size_t read_buf_size = 0;


void reset() {
    read_buf_size = 0;
    payload_size = 0;
}

void on_read() {
    if(read_buf_size == sizeof(payload_size)) {
        memcpy(&payload_size, &read_buf[0], sizeof(payload_size));
        if(payload_size > 10000) {
            const uint8_t transfer_done_msg[] =
                    "Bad payload size\r\n";
            print(transfer_done_msg, sizeof(transfer_done_msg));
            reset();
        } else {
            const uint8_t transfer_done_msg[] =
                    "Payload size received\r\n";
            print(transfer_done_msg, sizeof(transfer_done_msg));
        }
    }
    size_t full_msg_size = sizeof(payload_size) + payload_size + sizeof(uint16_t);
    if (read_buf_size == full_msg_size) {
        uint16_t crc16 = 0;
        memcpy(&crc16, &read_buf[read_buf_size - 2], sizeof(crc16));
        if(crc_ok(crc16)) {
            size_t bytes_transferred = transfer_to_fabric(read_buf, read_buf_size - 2);
            if (bytes_transferred != 0) {
                const uint8_t transfer_done_msg[] =
                        "Transferred to fabric\r\n";
                print(transfer_done_msg, sizeof(transfer_done_msg));
            } else {
                const uint8_t transfer_error_msg[] =
                        "Transfer to fabric failed\r\n";
                print(transfer_error_msg, sizeof(transfer_error_msg));
            }
        } else {
            const uint8_t crc_error_msg[] =
                    "Bad CRC\r\n";
            print(crc_error_msg, sizeof(crc_error_msg));
        }
        reset();
    }
}

void uart0_rx_handler() {
    uint8_t rx_byte = 0;
    uint8_t rx_size = MSS_UART_get_rx(&g_mss_uart0, &rx_byte, sizeof(rx_byte));
    if (rx_size == 1) {
        if (read_buf_size < sizeof(read_buf)) {
            read_buf[read_buf_size] = rx_byte;
            read_buf_size += 1;
            on_read();
        }
    }
}

void init_uart() {
    MSS_UART_init(&g_mss_uart0,
    MSS_UART_57600_BAUD,
    MSS_UART_DATA_8_BITS | MSS_UART_NO_PARITY | MSS_UART_ONE_STOP_BIT);
    MSS_UART_set_rx_handler(&g_mss_uart0, uart0_rx_handler,
            MSS_UART_FIFO_SINGLE_BYTE);
}

int main() {
    init_uart();
    const uint8_t uart_init_msg[] = "UART initialized\r\n";
    print(uart_init_msg, sizeof(uart_init_msg));

    for (;;) {
    }
    return 0;
}
