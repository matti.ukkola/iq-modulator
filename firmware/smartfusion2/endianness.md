# Example

Type following bytes into console:

```sh
12345678
```

After going to the `MCU`, the tx buffer looks like the following:

```
tx_buf[0] = '1'
tx_buf[1] = '2'
tx_buf[2] = '3'
tx_buf[3] = '4'
tx_buf[4] = '5'
tx_buf[5] = '6'
tx_buf[6] = '7'
tx_buf[7] = '8'
```

A little-endian  `uint32_t` looks like the following after a mem-copy:

```
const uint8_t buf[8] = {'1', '2', '3', '4', '5', '6', '7', '8'};
uint32_t val = 0;
std::memcpy(&val, &buf[0], sizeof(val));
uint8_t* left_most_byte_ptr = reinterpret_cast<uint8_t*>(&val);
std::cout << "Left most byte: "  << *left_most_byte_ptr << '\n';
```

prints:
```sh
Left most byte: 1
```

Therefore:
```
uint32_t val = "1234";
```

as bits:
```
uint32_t val = "bits('1'), bits('2'), bits('3'), bits('4')";
```

In VHDL we save the value as little endian with `L downto R`:
```vhdl
tx_bits : std_logic_vector(31 downto 0) := bits('1') & bits('2') & bits('3') & bits('4'));
```

If we index into `tx_bits` with 0, we get the LSB, or the last bit of bits('4') first on the transmission line.
