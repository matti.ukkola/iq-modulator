library IEEE;

use IEEE.std_logic_1164.all;

entity signal_mapper_logic is
port (
    --<port_name> : <direction> <type>;
	data_in : IN  std_logic;
    clk : IN std_logic;
    reset_n : IN std_logic;
    i_out : OUT std_logic;
    q_out : OUT std_logic;
    clk_out : OUT std_logic);
end signal_mapper_logic;
architecture architecture_signal_mapper_logic of signal_mapper_logic is
   signal prev_data : std_logic;
   signal clk_out_sig : std_logic;
begin
    process(clk, reset_n)
    begin
        if(reset_n = '0') then
            clk_out <= '0';
            clk_out_sig <= '1';
            i_out <= '0';
            q_out <= '0';
            prev_data <= '0'; -- clear memory on reset
        elsif(clk'EVENT and clk = '1') then
            --gray mapping: https://en.wikipedia.org/wiki/Phase-shift_keying#/media/File:QPSK_Gray_Coded.svg
            if(clk_out_sig = '1') then
                i_out <= prev_data;
                q_out <= data_in;
            else
                prev_data <= data_in;
            end if;
            clk_out_sig <= not clk_out_sig;
            clk_out <= clk_out_sig;
        end if;
    end process;
end architecture_signal_mapper_logic;
