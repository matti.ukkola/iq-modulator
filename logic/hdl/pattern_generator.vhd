library IEEE;

use IEEE.std_logic_1164.all;

entity pattern_generator is

generic(
    constant symbols : std_logic_vector(7 downto 0) := "00101101";
    constant N : integer := 8);

port(
    clk : in std_logic;
    reset_n : in std_logic;
    data_out : out std_logic);
end pattern_generator;

architecture architecture_pattern_generator of pattern_generator is
begin
    process(clk, reset_n)
        variable index : integer range 0 to N-1 := 0;
    begin
        if(reset_n = '0') then
            index := 0;
            data_out <= '0';
        elsif(clk'EVENT and clk = '1') then
            data_out <= symbols(index);
            if (index = N-1) then
                index := 0;
            else
                index := index + 1;
            end if;
        end if;
    end process;
end architecture_pattern_generator;
