
library IEEE;

use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.numeric_std_unsigned.all;

entity frame_to_bitstream is
port (
    -- Global Signals
    ACLK      : in std_logic;
    ACLK_fast : in std_logic;
    ARESETN   : in std_logic;

    -- in
	frame_in : in  std_logic_vector(31 downto 0);     -- frame input 32 bit
    PING_in  : in  std_logic;                         -- PING input
    
    -- out
    bitstream_out  : out std_logic;                   -- bitstream output
    reset_frame_out: out std_logic;     -- trigger reset to apb3 bus logic
    debug7_out     : out std_logic_vector(7 downto 0);-- debug LEDs out
    debug_STREAM   : out std_logic;                   -- debug LED out
    debug_SENDING  : out std_logic;                    -- check state
    debug_CLK      : out std_logic
);
end frame_to_bitstream;


architecture architecture_frame_to_bitstream of frame_to_bitstream is
   -- Final State Machine definition
    type states is (IDLE, SENDING);
    signal state        : states;
    signal input_state  : std_logic;
    
	signal frame        : std_logic_vector(31 downto 0); -- frame var (if input changes)

begin
    detecting_ping : process(ACLK_fast)
    begin
        if rising_edge(ACLK_fast) then 
            if PING_in = '1' then
                debug_SENDING <= '0'; -- LED an
            end if;
        end if;
    end process;
    

    unframing : process (ACLK, ARESETN) -- this cuts the frame in bits
    variable i: integer := 0; -- variable for if statement
    begin
        if ARESETN = '0' then
            -- Reset
            state <= IDLE;
            input_state <= '0';
            reset_frame_out <= '0';
            frame      <= x"00000000";
            i := 0;
            debug_SENDING <= '1'; -- LED aus
        elsif (rising_edge(ACLK)) then
            --debug_SENDING <= '1';
            case state is
            
                when IDLE =>
                    bitstream_out <= '0';
                    if frame_in /= x"00000000" then
                    --if input_state = '1' then
                        input_state <= '0';
                        state <= SENDING;
                        frame <= frame_in;
                        bitstream_out <= frame_in(i);
                        reset_frame_out <= '1';
                    end if;
                    
                when SENDING =>
                    reset_frame_out <= '0';
                    i := i + 1;
                    bitstream_out <= frame(i);
                    if i = 31 then
                        state <= IDLE;
                        frame <= x"00000000";
                        --if debug_SENDING = '1' then
                        --    debug_SENDING <= '0'; -- LED an
                        --else
                        --    debug_SENDING <= '1'; -- LED aus
                        --end if;
                        i := 0;
                    end if;

            end case;
        end if;
    end process;

debug_CLK <= ACLK;                   -- Debug clock putput           LED D9
debug_STREAM <= not bitstream_out;   -- routes bitstream out also to LED D2
debug7_out <= not frame(7 downto 0); -- shows (5 downto 1) at        LED D8 - D4

end architecture_frame_to_bitstream;