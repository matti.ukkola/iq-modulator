--------------------------------------------------------------------------------
-- Company: EMM
--
-- File: MRAMWriter.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- This VHDL works propperly at @ 150 MHz
--
-- Targeted device: <Family::PolarFireSoC> <Die::MPFS250T_ES> <Package::FCVG484>
-- Author: <Chedi Fassi>
--
--------------------------------------------------------------------------------

library IEEE;


use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.numeric_std_unsigned.all;
entity apb3_frame is

    port(
        -------------------------
		-- LED OUT
		-------------------------
        --LEDs : OUT std_logic_vector(3 downto 0);
		-------------------------
		-- RAM interface
		-------------------------
		--- Global signals
		ACLK                     : in  std_logic;
		ARESETN                  : in  std_logic;
        reset_frame_in           : in  std_logic;

        
        
        ---- APB Interface ----
        
        PSEL                     : in  std_logic;
        PWRITE                   : in  std_logic;
        PENABLE                  : in  std_logic;
        PSLVERR                  : out  std_logic;
        PREADY                   : out  std_logic;
		PADDR                    : in  std_logic_vector(31 downto 0);
        PWDATA                   : in  std_logic_vector(31 downto 0);
        PRDATA                   : out  std_logic_vector(31 downto 0);
        -----------------------
        
        --- out
        debug_out              : out  std_logic_vector(7 downto 0); -- Address Inputs A
        debugPING_out           : out std_logic; 
        PING                   : out std_logic;
        frame_out              : out std_logic_vector(31 downto 0) --NEW: output frame 32 bit
    );
end apb3_frame;

--Architecture Section

architecture architecture_apb3_frame of apb3_frame is
    type ram_states is (idle, write_reset1_state_L, write_state_H, write_reset1_state_H, reset2_state, read_reset_state_L, read_state_H, read_reset_state_H, wait_state);
    type test_states is (init, idle,  wait_s);
    signal state : ram_states;
    signal next_state : ram_states;
    signal state_t : test_states;
    signal state_t_next : test_states;
    signal count : integer :=0;
    
    signal rd_en : std_logic;
    signal wr_en : std_logic;
    signal w_data : std_logic_vector(31 downto 0);
    signal r_data : std_logic_vector(31 downto 0);
    signal addr : std_logic_vector(31 downto 0);
    signal pready_s : std_logic;
    signal tmp_data : std_logic_vector(31 downto 0);
    signal rvalue : std_logic_vector(31 downto 0);
    signal frames_send : std_logic_vector(31 downto 0);  -- increment after each frame send and give it out to LEDs as DEBUG info
    signal data_frame_1 : std_logic_vector(31 downto 0);  -- first data Frame which will be send over 
  
begin
  -- unused signals
PREADY <= '1';
PSLVERR <= '0';

    
  --                          PRDATA(15 downto 0) <= rvalue(15 downto 0);
    
test_states_proc : process (ACLK, ARESETN) -- this tests MRAM Function
    begin
        if ARESETN = '0' then
            state_t <= idle;
            state_t_next <= idle;
            w_data <= X"00000000"; 
            rd_en <= '0';
            wr_en <= '0';
            addr <= X"00000000";
            tmp_data <= X"00000000"; -- remove
            PING <= '0';
            --PRDATA <= X"00000000";
        elsif (rising_edge(ACLK)) then
            if reset_frame_in = '1' then
                w_data <= X"00000000"; 
            end if;
            case state_t is
                when init=>
                    if ( PSEL = '1' and PENABLE = '0') then
                        state_t <= idle;
                    end if;
                when idle =>
                    if ((PENABLE and PSEL) = '1' ) then
                    addr <= PADDR;
                    PING <= '1';
                    state_t <= wait_s;
                    state_t_next <= init;
                        if (PWRITE = '1') then
                            w_data <= PWDATA;
                            wr_en <= '1';
                            
                        else
                            rd_en <= '1';
                        end if;
                    end if;
                when wait_s =>

                        state_t <= state_t_next;
                        PING <= '0';
                    rd_en <= '0';
                    wr_en <= '0';

            end case;
        end if;
    end process;
    
frame_out <= w_data;
debug_out <= not w_data(7 downto 0);
-- debugPING_out <= not PING;


end architecture_apb3_frame;
