new_project -location {./IQModulator} -name {IQModulator} -project_description {} -block_mode 0 -standalone_peripheral_initialization 0 -instantiate_in_smartdesign 1 -ondemand_build_dh 1 -use_relative_path 0 -linked_files_root_dir_env {} -hdl {VHDL} -family {SmartFusion2} -die {M2S010} -package {144 TQ} -speed {STD} -die_voltage {1.2} -part_range {COM} -adv_options {DSW_VCCA_VOLTAGE_RAMP_RATE:100_MS} -adv_options {IO_DEFT_STD:LVCMOS 2.5V} -adv_options {PLL_SUPPLY:PLL_SUPPLY_25} -adv_options {RESTRICTPROBEPINS:1} -adv_options {RESTRICTSPIPINS:0} -adv_options {SYSTEM_CONTROLLER_SUSPEND_MODE:0} -adv_options {TEMPR:COM} -adv_options {VCCI_1.2_VOLTR:COM} -adv_options {VCCI_1.5_VOLTR:COM} -adv_options {VCCI_1.8_VOLTR:COM} -adv_options {VCCI_2.5_VOLTR:COM} -adv_options {VCCI_3.3_VOLTR:COM} -adv_options {VOLTR:COM}

# Add main smart design component
create_smartdesign -sd_name {IQModulator}
set_root -module {IQModulator::work}

# Make HDL Files Available
import_files \
         -convert_EDN_to_HDL 0 \
         -hdl_source {./hdl/APB3_frame.vhd} \
         -hdl_source {./hdl/clk_div_n.vhd} \
         -hdl_source {./hdl/frame_to_bitstream.vhd} \
         -hdl_source {./hdl/pattern_generator.vhd} \
         -hdl_source {./hdl/signal_mapper_logic.vhd} \
         -io_pdc {./constraints/io/user.pdc}

# Add MSS
create_and_configure_core -core_vlnv {Actel:SmartFusion2MSS:MSS:1.1.500} -component_name {IQModulator_MSS} -params {}

# Disable unused components
mss_disable_instance -component_name {IQModulator_MSS} -instance_name {FIC32_1}
mss_disable_instance -component_name {IQModulator_MSS} -instance_name {RTC}
mss_disable_instance -component_name {IQModulator_MSS} -instance_name {WATCHDOG}
mss_disable_instance -component_name {IQModulator_MSS} -instance_name {DMA}
mss_disable_instance -component_name {IQModulator_MSS} -instance_name {CAN}
mss_disable_instance -component_name {IQModulator_MSS} -instance_name {I2C_0}
mss_disable_instance -component_name {IQModulator_MSS} -instance_name {I2C_1}
mss_disable_instance -component_name {IQModulator_MSS} -instance_name {SPI_0}
mss_disable_instance -component_name {IQModulator_MSS} -instance_name {SPI_1}
mss_disable_instance -component_name {IQModulator_MSS} -instance_name {MMUART_1}
mss_disable_instance -component_name {IQModulator_MSS} -instance_name {MAC}
mss_disable_instance -component_name {IQModulator_MSS} -instance_name {USB}

# Configure MSS Clocks
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {CCC} -params {"FAMILY:19"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {CCC} -params {"REFCLK_SRC:CLK_BASE"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {CCC} -params {"REFCLK_FREQ:100"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {CCC} -params {"MPLL_SUPPLY_VOLTAGE:3_3"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {CCC} -params {"MPLL_LOCK_COUNT:32"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {CCC} -params {"MPLL_LOCK_WINDOW:8000"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {CCC} -params {"MDDR_FCLK_MULT:2"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {CCC} -params {"FCLK_FREQ:100"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {CCC} -params {"FCLK_PCLK0_DIVISOR:1"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {CCC} -params {"FCLK_PCLK1_DIVISOR:1"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {CCC} -params {"MDDR_CLK_FIC64_DIVISOR:1"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {CCC} -params {"FIC32_0_DIVISOR:1"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {CCC} -params {"FIC32_1_DIVISOR:1"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {CCC} -params {"MPLL_LOCK_IS_USED:false"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {CCC} -params {"FACC_GLMUX_SEL_IS_USED:false"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {CCC} -params {"FAB_PLL_LOCK_IS_USED:1"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {CCC} -params {"MPLL_LOCK_EN:false"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {CCC} -params {"MPLL_LOCK_LOST_EN:false"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {CCC} -params {"FAB_PLL_LOCK_EN:false"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {CCC} -params {"FAB_PLL_LOCK_LOST_EN:false"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {CCC} -params {"PARAM_IS_FALSE:false"} -validate_rules 0
sd_save_core_instance_config -sd_name {IQModulator_MSS} -instance_name {CCC}

# Configure MSS FIC 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {FIC32_0} -params {"FAMILY:19"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {FIC32_0} -params {"PARAM_IS_FALSE:false"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {FIC32_0} -params {"MSS_CLK_FREQUENCY:100.000"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {FIC32_0} -params {"FAB_CLK_FREQUENCY:100.000"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {FIC32_0} -params {"INTERFACE_TYPE:INTERFACE_APB"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {FIC32_0} -params {"USE_BYPASS_MODE:false"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {FIC32_0} -params {"INTERFACE_MASTER:1"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {FIC32_0} -params {"INTERFACE_SLAVE:false"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {FIC32_0} -params {"EXPOSE_MASTER_IDENTITY:false"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {FIC32_0} -params {"USE_FAB_REGION_0:true"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {FIC32_0} -params {"USE_FAB_REGION_1:true"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {FIC32_0} -params {"USE_FAB_REGION_2:true"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {FIC32_0} -params {"USE_FAB_REGION_3:false"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {FIC32_0} -params {"USE_FAB_REGION_4:false"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {FIC32_0} -params {"USE_FAB_REGION_5:false"} -validate_rules 0
sd_save_core_instance_config -sd_name {IQModulator_MSS} -instance_name {FIC32_0}

# Configure MSS APB Bus
sd_rename_port -sd_name {IQModulator_MSS} -current_port_name {APB_MASTER} -new_port_name {FIC_0_APB_MASTER}
sd_rename_port -sd_name {IQModulator_MSS} -current_port_name {APB_M_PADDR} -new_port_name {FIC_0_APB_M_PADDR}
sd_rename_port -sd_name {IQModulator_MSS} -current_port_name {APB_M_PSEL} -new_port_name {FIC_0_APB_M_PSEL}
sd_rename_port -sd_name {IQModulator_MSS} -current_port_name {APB_M_PENABLE} -new_port_name {FIC_0_APB_M_PENABLE}
sd_rename_port -sd_name {IQModulator_MSS} -current_port_name {APB_M_PWRITE} -new_port_name {FIC_0_APB_M_PWRITE}
sd_rename_port -sd_name {IQModulator_MSS} -current_port_name {APB_M_PRDATA} -new_port_name {FIC_0_APB_M_PRDATA}
sd_rename_port -sd_name {IQModulator_MSS} -current_port_name {APB_M_PWDATA} -new_port_name {FIC_0_APB_M_PWDATA}
sd_rename_port -sd_name {IQModulator_MSS} -current_port_name {APB_M_PREADY} -new_port_name {FIC_0_APB_M_PREADY}
sd_rename_port -sd_name {IQModulator_MSS} -current_port_name {APB_M_PSLVERR} -new_port_name {FIC_0_APB_M_PSLVERR}
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {RESET} -params {"PARAM_IS_FALSE:false"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {RESET} -params {"USER_MSS_RESET_N_USED:true"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {RESET} -params {"FAB_M3_RESET_N_USED:false"} -validate_rules 0
sd_configure_core_instance -sd_name {IQModulator_MSS} -instance_name {RESET} -params {"FPGA_RESET_N_USED:false"} -validate_rules 0
sd_save_core_instance_config -sd_name {IQModulator_MSS} -instance_name {RESET}

# Add MSS to SmartDesign
save_smartdesign -sd_name {IQModulator_MSS}
generate_component -component_name {IQModulator_MSS} -recursive 0
sd_instantiate_component -sd_name {IQModulator} -component_name {IQModulator_MSS} -instance_name {IQModulator_MSS_0}

# Configure MSS UART 0
sd_connect_pin_to_port -sd_name {IQModulator} -pin_name {IQModulator_MSS_0:MMUART_0_RXD_F2M} -port_name {}
sd_connect_pin_to_port -sd_name {IQModulator} -pin_name {IQModulator_MSS_0:MMUART_0_TXD_M2F} -port_name {}

# Configure Clocks
create_and_configure_core -core_vlnv {Actel:SgCore:OSC:2.0.101} -component_name {OSC_C0} -params {"RCOSC_1MHZ_DRIVES_CCC:false" "RCOSC_1MHZ_DRIVES_FAB:false" "RCOSC_1MHZ_IS_USED:false" "RCOSC_25_50MHZ_DRIVES_CCC:1" "RCOSC_25_50MHZ_DRIVES_FAB:0" "RCOSC_25_50MHZ_IS_USED:1" "VOLTAGE_IS_1_2:true" "XTLOSC_DRIVES_CCC:false" "XTLOSC_DRIVES_FAB:false" "XTLOSC_FREQ:20.00" "XTLOSC_IS_USED:false" "XTLOSC_SRC:CRYSTAL"}
sd_instantiate_component -sd_name {IQModulator} -component_name {OSC_C0} -instance_name {}
create_and_configure_core -core_vlnv {Actel:SgCore:FCCC:2.0.201} -component_name {FCCC_C0} -params {"ADVANCED_TAB_CHANGED:false" "CLK0_IS_USED:false" "CLK0_PAD_IS_USED:false" "CLK1_IS_USED:false" "CLK1_PAD_IS_USED:false" "CLK2_IS_USED:false" "CLK2_PAD_IS_USED:false" "CLK3_IS_USED:false" "CLK3_PAD_IS_USED:false" "DYN_CONF_IS_USED:false" "GL0_BP_IN_0_FREQ:100" "GL0_BP_IN_0_SRC:IO_HARDWIRED_0" "GL0_BP_IN_1_FREQ:100" "GL0_BP_IN_1_SRC:IO_HARDWIRED_0" "GL0_FREQUENCY_LOCKED:false" "GL0_IN_0_SRC:PLL" "GL0_IN_1_SRC:UNUSED" "GL0_IS_INVERTED:false" "GL0_IS_USED:true" "GL0_OUT_0_FREQ:100" "GL0_OUT_1_FREQ:50" "GL0_OUT_IS_GATED:false" "GL0_PLL_IN_0_PHASE:0" "GL0_PLL_IN_1_PHASE:0" "GL1_BP_IN_0_FREQ:100" "GL1_BP_IN_0_SRC:IO_HARDWIRED_0" "GL1_BP_IN_1_FREQ:100" "GL1_BP_IN_1_SRC:IO_HARDWIRED_0" "GL1_FREQUENCY_LOCKED:false" "GL1_IN_0_SRC:PLL" "GL1_IN_1_SRC:UNUSED" "GL1_IS_INVERTED:false" "GL1_IS_USED:false" "GL1_OUT_0_FREQ:100" "GL1_OUT_1_FREQ:50" "GL1_OUT_IS_GATED:false" "GL1_PLL_IN_0_PHASE:0" "GL1_PLL_IN_1_PHASE:0" "GL2_BP_IN_0_FREQ:100" "GL2_BP_IN_0_SRC:IO_HARDWIRED_0" "GL2_BP_IN_1_FREQ:100" "GL2_BP_IN_1_SRC:IO_HARDWIRED_0" "GL2_FREQUENCY_LOCKED:false" "GL2_IN_0_SRC:PLL" "GL2_IN_1_SRC:UNUSED" "GL2_IS_INVERTED:false" "GL2_IS_USED:false" "GL2_OUT_0_FREQ:100" "GL2_OUT_1_FREQ:50" "GL2_OUT_IS_GATED:false" "GL2_PLL_IN_0_PHASE:0" "GL2_PLL_IN_1_PHASE:0" "GL3_BP_IN_0_FREQ:100" "GL3_BP_IN_0_SRC:IO_HARDWIRED_0" "GL3_BP_IN_1_FREQ:100" "GL3_BP_IN_1_SRC:IO_HARDWIRED_0" "GL3_FREQUENCY_LOCKED:false" "GL3_IN_0_SRC:PLL" "GL3_IN_1_SRC:UNUSED" "GL3_IS_INVERTED:false" "GL3_IS_USED:false" "GL3_OUT_0_FREQ:100" "GL3_OUT_1_FREQ:50" "GL3_OUT_IS_GATED:false" "GL3_PLL_IN_0_PHASE:0" "GL3_PLL_IN_1_PHASE:0" "GPD0_IS_USED:false" "GPD0_NOPIPE_RSTSYNC:true" "GPD0_SYNC_STYLE:G3STYLE_AND_NO_LOCK_RSTSYNC" "GPD1_IS_USED:false" "GPD1_NOPIPE_RSTSYNC:true" "GPD1_SYNC_STYLE:G3STYLE_AND_NO_LOCK_RSTSYNC" "GPD2_IS_USED:false" "GPD2_NOPIPE_RSTSYNC:true" "GPD2_SYNC_STYLE:G3STYLE_AND_NO_LOCK_RSTSYNC" "GPD3_IS_USED:false" "GPD3_NOPIPE_RSTSYNC:true" "GPD3_SYNC_STYLE:G3STYLE_AND_NO_LOCK_RSTSYNC" "GPD_EXPOSE_RESETS:false" "GPD_SYNC_STYLE:G3STYLE_AND_LOCK_RSTSYNC" "INIT:0000007FB8000044D74000318C6318C1F18C61EC0404040400301" "IO_HARDWIRED_0_IS_DIFF:false" "IO_HARDWIRED_1_IS_DIFF:false" "IO_HARDWIRED_2_IS_DIFF:false" "IO_HARDWIRED_3_IS_DIFF:false" "MODE_10V:false" "NGMUX0_HOLD_IS_USED:false" "NGMUX0_IS_USED:false" "NGMUX1_HOLD_IS_USED:false" "NGMUX1_IS_USED:false" "NGMUX2_HOLD_IS_USED:false" "NGMUX2_IS_USED:false" "NGMUX3_HOLD_IS_USED:false" "NGMUX3_IS_USED:false" "NGMUX_EXPOSE_HOLD:false" "PLL_DELAY:0" "PLL_EXPOSE_BYPASS:false" "PLL_EXPOSE_RESETS:false" "PLL_EXT_FB_GL:EXT_FB_GL0" "PLL_FB_SRC:CCC_INTERNAL" "PLL_IN_FREQ:50.000" "PLL_IN_SRC:OSC_50MHZ" "PLL_IS_USED:true" "PLL_LOCK_IND:1024" "PLL_LOCK_WND:32000" "PLL_SSM_DEPTH:0.5" "PLL_SSM_ENABLE:false" "PLL_SSM_FREQ:40" "PLL_SUPPLY_VOLTAGE:25_V" "PLL_VCO_TARGET:700" "RCOSC_1MHZ_IS_USED:false" "RCOSC_25_50MHZ_IS_USED:true" "VCOFREQUENCY:800.000" "XTLOSC_IS_USED:false" "Y0_IS_USED:false" "Y1_IS_USED:false" "Y2_IS_USED:false" "Y3_IS_USED:false"}
sd_instantiate_component -sd_name {IQModulator} -component_name {FCCC_C0} -instance_name {}
sd_connect_pins -sd_name {IQModulator} -pin_names {"FCCC_C0_0:RCOSC_25_50MHZ_CCC_IN" "OSC_C0_0:RCOSC_25_50MHZ_CCC_OUT"}
sd_connect_pins -sd_name {IQModulator} -pin_names {"FCCC_C0_0:GL0" "IQModulator_MSS_0:MCCC_CLK_BASE"}
sd_connect_pins -sd_name {IQModulator} -pin_names {"FCCC_C0_0:LOCK" "IQModulator_MSS_0:MCCC_CLK_BASE_PLL_LOCK"}

# Configure Reset
sd_instantiate_macro -sd_name {IQModulator} -macro_name {SYSRESET} -instance_name {SYSRESET_0}
sd_connect_pins -sd_name {IQModulator} -pin_names {"IQModulator_MSS_0:MSS_RESET_N_F2M" "SYSRESET_0:POWER_ON_RESET_N"}

# Basic config complete, save progress
generate_component -component_name {IQModulator} -recursive 0
build_design_hierarchy

# Configure APB3
create_hdl_core -file {./hdl/APB3_frame.vhd} -module {apb3_frame}
hdl_core_add_bif -hdl_core_name {apb3_frame} -bif_definition {APB:AMBA:AMBA2:slave} -bif_name {BIF_1} -signal_map {}
hdl_core_assign_bif_signal -hdl_core_name {apb3_frame} -bif_name {BIF_1} -bif_signal_name {PADDR} -core_signal_name {PADDR}
hdl_core_assign_bif_signal -hdl_core_name {apb3_frame} -bif_name {BIF_1} -bif_signal_name {PENABLE} -core_signal_name {PENABLE}
hdl_core_assign_bif_signal -hdl_core_name {apb3_frame} -bif_name {BIF_1} -bif_signal_name {PWRITE} -core_signal_name {PWRITE}
hdl_core_assign_bif_signal -hdl_core_name {apb3_frame} -bif_name {BIF_1} -bif_signal_name {PRDATA} -core_signal_name {PRDATA}
hdl_core_assign_bif_signal -hdl_core_name {apb3_frame} -bif_name {BIF_1} -bif_signal_name {PWDATA} -core_signal_name {PWDATA}
hdl_core_assign_bif_signal -hdl_core_name {apb3_frame} -bif_name {BIF_1} -bif_signal_name {PREADY} -core_signal_name {PREADY}
hdl_core_assign_bif_signal -hdl_core_name {apb3_frame} -bif_name {BIF_1} -bif_signal_name {PSLVERR} -core_signal_name {PSLVERR}
hdl_core_assign_bif_signal -hdl_core_name {apb3_frame} -bif_name {BIF_1} -bif_signal_name {PSELx} -core_signal_name {PSEL}

sd_instantiate_hdl_core -sd_name {IQModulator} -hdl_core_name {apb3_frame} -instance_name {}
sd_mark_pins_unused -sd_name {IQModulator} -pin_names {apb3_frame_0:debug_out}
sd_mark_pins_unused -sd_name {IQModulator} -pin_names {apb3_frame_0:debugPING_out}
sd_mark_pins_unused -sd_name {IQModulator} -pin_names {apb3_frame_0:frame_out}
sd_connect_pins -sd_name {IQModulator} -pin_names {"SYSRESET_0:POWER_ON_RESET_N" "apb3_frame_0:ARESETN"}

create_and_configure_core -core_vlnv {Actel:DirectCore:CoreAPB3:4.2.100} -component_name {CoreAPB3_C0} -params {"APBSLOT0ENABLE:false" "APBSLOT10ENABLE:false" "APBSLOT11ENABLE:false" "APBSLOT12ENABLE:false" "APBSLOT13ENABLE:false" "APBSLOT14ENABLE:false" "APBSLOT15ENABLE:false" "APBSLOT1ENABLE:false" "APBSLOT2ENABLE:false" "APBSLOT3ENABLE:true" "APBSLOT4ENABLE:false" "APBSLOT5ENABLE:false" "APBSLOT6ENABLE:false" "APBSLOT7ENABLE:false" "APBSLOT8ENABLE:false" "APBSLOT9ENABLE:false" "APB_DWIDTH:32" "IADDR_OPTION:0" "MADDR_BITS:32" "SC_0:false" "SC_10:false" "SC_11:false" "SC_12:false" "SC_13:false" "SC_14:false" "SC_15:false" "SC_1:false" "SC_2:false" "SC_3:false" "SC_4:false" "SC_5:false" "SC_6:false" "SC_7:false" "SC_8:false" "SC_9:false" "UPR_NIBBLE_POSN:6"}
sd_instantiate_component -sd_name {IQModulator} -component_name {CoreAPB3_C0} -instance_name {}

sd_connect_pins -sd_name {IQModulator} -pin_names {"CoreAPB3_C0_0:APB3mmaster" "IQModulator_MSS_0:FIC_0_APB_MASTER"}
sd_connect_pins -sd_name {IQModulator} -pin_names {"CoreAPB3_C0_0:APBmslave3" "apb3_frame_0:BIF_1"}

#Configure clock division
sd_instantiate_hdl_module -sd_name {IQModulator} -hdl_module_name {clk_div_n} -hdl_file {hdl/clk_div_n.vhd} -instance_name {}
sd_connect_pins -sd_name {IQModulator} -pin_names {"FCCC_C0_0:GL0" "clk_div_n_0:clk"}
sd_connect_pins -sd_name {IQModulator} -pin_names {"SYSRESET_0:POWER_ON_RESET_N" "clk_div_n_0:reset_n"}

# Configure Frame to Bistream
sd_instantiate_hdl_module -sd_name {IQModulator} -hdl_module_name {frame_to_bitstream} -hdl_file {hdl/frame_to_bitstream.vhd} -instance_name {}
sd_connect_pins -sd_name {IQModulator} -pin_names {"apb3_frame_0:PING" "frame_to_bitstream_0:PING_in"}
sd_clear_pin_attributes -sd_name {IQModulator} -pin_names {apb3_frame_0:frame_out}
sd_connect_pins -sd_name {IQModulator} -pin_names {"apb3_frame_0:frame_out" "frame_to_bitstream_0:frame_in"}
sd_connect_pins -sd_name {IQModulator} -pin_names {"SYSRESET_0:POWER_ON_RESET_N" "frame_to_bitstream_0:ARESETN"}
sd_connect_pins -sd_name {IQModulator} -pin_names {"FCCC_C0_0:GL0" "apb3_frame_0:ACLK"}
sd_connect_pins -sd_name {IQModulator} -pin_names {"FCCC_C0_0:GL0" "frame_to_bitstream_0:ACLK_fast"}
sd_connect_pins -sd_name {IQModulator} -pin_names {"frame_to_bitstream_0:reset_frame_out" "apb3_frame_0:reset_frame_in"}
sd_connect_pins -sd_name {IQModulator} -pin_names {"clk_div_n_0:clk_out" "frame_to_bitstream_0:ACLK"}
sd_mark_pins_unused -sd_name {IQModulator} -pin_names {frame_to_bitstream_0:debug7_out}
sd_mark_pins_unused -sd_name {IQModulator} -pin_names {frame_to_bitstream_0:debug_SENDING}
sd_connect_pin_to_port -sd_name {IQModulator} -pin_name {frame_to_bitstream_0:debug_STREAM} -port_name {}
sd_connect_pin_to_port -sd_name {IQModulator} -pin_name {frame_to_bitstream_0:debug_CLK} -port_name {}

# Configure Signal Mapper
sd_instantiate_hdl_module -sd_name {IQModulator} -hdl_module_name {signal_mapper_logic} -hdl_file {hdl/signal_mapper_logic.vhd} -instance_name {}
sd_connect_pins -sd_name {IQModulator} -pin_names {"clk_div_n_0:clk_out" "signal_mapper_logic_0:clk"}
sd_connect_pins -sd_name {IQModulator} -pin_names {"SYSRESET_0:POWER_ON_RESET_N" "signal_mapper_logic_0:reset_n"}
sd_connect_pins -sd_name {IQModulator} -pin_names {"frame_to_bitstream_0:bitstream_out" "signal_mapper_logic_0:data_in"}
sd_connect_pin_to_port -sd_name {IQModulator} -pin_name {signal_mapper_logic_0:i_out} -port_name {}
sd_connect_pin_to_port -sd_name {IQModulator} -pin_name {signal_mapper_logic_0:q_out} -port_name {}
sd_connect_pin_to_port -sd_name {IQModulator} -pin_name {signal_mapper_logic_0:clk_out} -port_name {}

generate_component -component_name {IQModulator} -recursive 0
build_design_hierarchy
sd_reset_layout -sd_name {IQModulator}
save_project
